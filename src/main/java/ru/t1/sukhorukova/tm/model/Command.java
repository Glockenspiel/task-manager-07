package ru.t1.sukhorukova.tm.model;

import ru.t1.sukhorukova.tm.constant.ArgumentConst;
import ru.t1.sukhorukova.tm.constant.CommandConst;

public class Command {

    public static final Command CMD_HELP = new Command(CommandConst.CMD_HELP, ArgumentConst.CMD_HELP, "Show list arguments.");
    public static final Command CMD_VERSION = new Command(CommandConst.CMD_VERSION, ArgumentConst.CMD_VERSION, "Show version.");
    public static final Command CMD_ABOUT = new Command(CommandConst.CMD_ABOUT, ArgumentConst.CMD_ABOUT, "Show about developer.");
    public static final Command CMD_INFO = new Command(CommandConst.CMD_INFO, ArgumentConst.CMD_INFO, "Show system information.");
    public static final Command CMD_EXIT = new Command(CommandConst.CMD_EXIT, null, "Close application.");

    private String name;
    private String argument;
    private String description;

    public Command() {
    }

    public Command(String name, String argument, String description) {
        this.name = name;
        this.argument = argument;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArgument() {
        return argument;
    }

    public void setArgument(String argument) {
        this.argument = argument;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        String displayName = "";
        if (name != null && !name.isEmpty()) displayName += name;
        if (argument != null && !argument.isEmpty()) displayName += ", " + argument;
        if (description != null && !description.isEmpty()) displayName += ": " + description;
        return displayName;
    }

}
